# Copyright 2009-2017 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require udev-rules [ udev_files=[ src/92_pcscd_${PN}.rules ] ]

SUMMARY="CCID free software driver"
DESCRIPTION="
This package provides the source code for a generic USB CCID (Chip/Smart Card Interface Devices)
driver and ICCD (Integrated Circuit(s) Card Devices).
"
HOMEPAGE="https://ccid.apdu.fr"
DOWNLOADS="${HOMEPAGE}/files/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/flex
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/libusb:1[>=1.0.9]
        sys-apps/pcsc-lite[>=1.8.3]
    run:
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libusb
    --enable-pcsclite
    --enable-twinserial
    --disable-embedded
    --disable-oslog
    --disable-zlp
)

src_compile() {
    default

    emake -C contrib/Kobil_mIDentity_switch
}

src_install() {
    default

    install_udev_files

    dobin contrib/Kobil_mIDentity_switch/Kobil_mIDentity_switch
    doman contrib/Kobil_mIDentity_switch/Kobil_mIDentity_switch.8
}

