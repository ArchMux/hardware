# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=tpm2-software release=${PV} suffix=tar.gz ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require systemd-service udev-rules

SUMMARY="TPM2 Software Stack"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+run:
        dev-libs/json-c:=
        net-misc/curl
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
        group/tss
        user/tss
    test:
        dev-util/cmocka
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Don-t-try-to-create-user-group.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --disable-defaultflags
    --disable-doxygen-rtf
    --disable-integration
    --disable-static
    --disable-valgrind
    --enable-fapi
    # mbedtls is also a possibilty, but doesn't work with FAPI
    --with-crypto=ossl
    --with-fuzzing=none
    --with-sysusersdir="/usr/$(exhost --target)/lib/sysusers.d"
    --with-tmpfilesdir=${SYSTEMDTMPFILESDIR}
    --with-udevrulesdir=${UDEVRULESDIR}
    --with-udevrulesprefix=60-
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc doxygen-doc'
    'doc doxygen-dot'
    'doc doxygen-man'
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-unit --disable-unit'
)

src_prepare() {
    # fix missing version in generated pkg-config files
    edo sed \
        -e "s:m4_esyscmd_s(\[git describe --tags --always --dirty\]):${PV}:g" \
        -i configure.ac

    autotools_src_prepare
}

src_install() {
    default

    option doc && dodoc -r doxygen-doc/html
}

